import socket
import json

addr = "192.168.7.77"
port = 36330

fahsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
fahsock.connect((addr, port))
data = fahsock.recv(4096)
fahsock.sendall(b"slot-info\n")
data = fahsock.recv(4096)
fahsock.close()

"""
this function was mostly taken and adapted from here:
https://github.com/FoldingAtHome/fah-control/blob/7016206a9e623ae95d78440035fa87c8319a2a8f/fah/Connection.py
"""


def parse_message(self, version, type, data):
    try:
        data = PYON_to_JSON(data)
        msg = json.loads(data)
        return msg
    except Exception as e:
        print(
            "ERROR parsing PyON message: %s: %s"
            % (str(e), data.encode("unicode_escape"))
        )


"""
this function was mostly taken and adapted from here:
https://github.com/FoldingAtHome/fah-control/blob/7016206a9e623ae95d78440035fa87c8319a2a8f/fah/Connection.py
"""


def parse(self):
    start = self.find("\nPyON ")
    msg = ""
    if start != -1:
        eol = self.find("\n", start + 1)
        if eol != -1:
            line = self[start + 1 : eol]
            tokens = line.split(None, 2)

            if len(tokens) < 3:
                self = self[eol:]
                raise Exception("Invalid PyON line: " + line.encode("unicode_escape"))

            version = int(tokens[1])
            type = tokens[2]

            end = self.find("\n---\n", start)
            if end != -1:
                data = self[eol + 1 : end]
                msg = parse_message(self, version, type, data)
                self = self[end + 4 :]
                return msg
    return ""


def PYON_to_JSON(data):
    data = data.replace("False", "false")
    data = data.replace("True", "true")
    data = data.replace("None", "null")
    return data


data = data.decode("utf-8")
value = parse(data)
print(json.dumps(value, indent=4))
