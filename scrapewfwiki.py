import requests
from bs4 import BeautifulSoup

# URL = "https://warframe.fandom.com/wiki/Dakra_Prime"
# URL = "https://warframe.fandom.com/wiki/Skana"
# URL = "https://warframe.fandom.com/wiki/Exergis"
URL = "http://warframe.fandom.com/wiki/Jat_Kusar"
headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0"
}

page = requests.get(URL, headers=headers)

soup = BeautifulSoup(page.content, "html.parser")


nameelem = soup.find("h2", {"class": "pi-item pi-item-spacing pi-title"})
nameval = nameelem.find("span").decode_contents()

masteryelem = soup.find("div", {"data-source": "mastery level"})
masteryval = masteryelem.find(
    "div", {"class": "pi-data-value pi-font"}
).decode_contents()

slotelem = soup.find("div", {"data-source": "slot"})
slotval = slotelem.find("div", {"class": "pi-data-value pi-font"}).decode_contents()

try:
    typeelem = soup.find("div", {"data-source": "type"})
    typevalmid = typeelem.find("div", {"class": "pi-data-value pi-font"})
    typeval = typevalmid.find("a").get_text()
except:
    pass

print("Name:", nameval)
print("Mastery Rank Required:", masteryval)
print("Slot:", slotval)
try:
    print("Type:", typeval)
except:
    pass

firerateelem = soup.find("div", {"data-source": "fire rate"})
firerateval = firerateelem.find(
    "div", {"class": "pi-data-value pi-font"}
).decode_contents()

try:
    findmgelem = soup.find("div", {"data-source": "finisher damage"})
    findmgval = findmgelem.find(
        "div", {"class": "pi-data-value pi-font"}
    ).decode_contents()
except:
    pass

try:
    chandmgelem = soup.find("div", {"data-source": "channel damage"})
    chandmgval = chandmgelem.find(
        "div", {"class": "pi-data-value pi-font"}
    ).decode_contents()

    blokreselem = soup.find("div", {"data-source": "block resist"})
    blokresval = blokreselem.find(
        "div", {"class": "pi-data-value pi-font"}
    ).decode_contents()

except:
    pass


try:
    impactelem = soup.find("td", {"data-source": "normal impact"}).get_text()
    impactval = impactelem.strip()
except:
    pass

try:
    punctelem = soup.find("td", {"data-source": "normal puncture"}).get_text()
    punctval = punctelem.strip()
except:
    pass

try:
    slashelem = soup.find("td", {"data-source": "normal slash"}).get_text()
    slashval = slashelem.strip()
except:
    pass

try:
    totaldmgelem = soup.find("div", {"data-source": "normal damage"})
    totaldmgmid = totaldmgelem.find(
        "div", {"class": "pi-data-value pi-font"}
    ).get_text()
    totaldmgval = totaldmgmid[0:4].strip()  # needs fix so no parentheses appears
except:
    pass

print("Fire Rate:", firerateval)
try:
    print("Finisher Damage:", findmgval)
except:
    pass
try:
    print("Channeling Damage:", chandmgval)
except:
    pass
try:
    print("Block Resist:", blokresval)
except:
    pass
try:
    print("Impact:", impactval)
except:
    pass
try:
    print("Puncture:", punctval)
except:
    pass
try:
    print("Slash:", slashval)
except:
    pass
try:
    print("Total Damage:", totaldmgval)
except:
    pass

critchelem = soup.find("div", {"data-source": "normal critical chance"})
critchval = (
    critchelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
)

critdmgelem = soup.find("div", {"data-source": "normal critical damage"})
critdmgval = (
    critdmgelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
)

statuschelem = soup.find("div", {"data-source": "normal status chance"})
statuschval = (
    statuschelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
)

try:
    normelemelem = soup.find("td", {"data-source": "normal element"}).get_text()
    normelemval = normelemelem.strip()
except:
    pass

try:
    slamelem = soup.find("div", {"data-source": "slam attack"})
    slamval = (
        slamelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
    )
    slideelem = soup.find("div", {"data-source": "slide attack"})
    slideval = (
        slideelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
    )
    wallelem = soup.find("div", {"data-source": "wall attack"})
    wallval = (
        wallelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
    )
except:
    pass

print("Critical Chance:", critchval)
print("Critical Damage:", critdmgval)
print("Status Chance:", statuschval)
try:
    print("Slam Attack:", slamval)
    print("Slide Attack:", slideval)
    print("Wall Attack:", wallval)
except:
    pass
try:
    print("Normal Element Damage", normelemval)
except:
    pass

try:
    whatelem = soup.find("td", {"data-source": "normal element"})
    whatelemval = whatelem.find("span", {"class": "damagetype-tooltip"})
except:
    pass

try:
    print(whatelemval["data-params"], normelemval)
except:
    pass
