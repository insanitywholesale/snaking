# Generated by Django 2.2.5 on 2019-09-15 04:51

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ("main", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="tutorial",
            name="tutorial_published",
            field=models.DateTimeField(
                default=datetime.datetime(2019, 9, 15, 4, 51, 42, 341345, tzinfo=utc),
                verbose_name="date uploaded",
            ),
        ),
    ]
