# Generated by Django 2.2.6 on 2019-10-13 10:49

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ("main", "0010_auto_20191013_1043"),
    ]

    operations = [
        migrations.AlterField(
            model_name="tutorial",
            name="tutorial_published",
            field=models.DateTimeField(
                default=datetime.datetime(2019, 10, 13, 10, 49, 40, 934177, tzinfo=utc),
                verbose_name="date uploaded",
            ),
        ),
    ]
