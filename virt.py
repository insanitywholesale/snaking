import libvirt
import sys

conn = libvirt.open("qemu:///system")  # ("qemu+ssh://root@mladyserver.hell/system")

if conn == None:
    print("Failed to open connection to the hypervisor")
    sys.exit(1)

activeDomainIDs = conn.listDomainsID()
activeDomainIDs.sort()

print(f"domain ID   domain UUID")
i = 0
for domainID in activeDomainIDs:
    domain = conn.lookupByID(domainID)
    uuid = domain.UUIDString()
    if activeDomainIDs[i] < 10:
        print(f"{activeDomainIDs[i]}    {uuid}")
    elif activeDomainIDs[i] < 100:
        print(f"{activeDomainIDs[i]}   {uuid}")
    else:
        print(f"{activeDomainIDs[i]}  {uuid}")
    i += 1
