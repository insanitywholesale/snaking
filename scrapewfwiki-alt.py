import requests
from bs4 import BeautifulSoup

URL = "https://warframe.fandom.com/wiki/Dakra_Prime"
# URL = "https://warframe.fandom.com/wiki/Skana"
# URL = "https://warframe.fandom.com/wiki/Exergis"
headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0"
}

page = requests.get(URL, headers=headers)

soup = BeautifulSoup(page.content, "html.parser")

nameelem = soup.find("h2", {"class": "pi-item pi-item-spacing pi-title"})
nameval = nameelem.find("span").decode_contents()

masteryelem = soup.find("div", {"data-source": "mastery level"})
masteryval = masteryelem.find(
    "div", {"class": "pi-data-value pi-font"}
).decode_contents()

slotelem = soup.find("div", {"data-source": "slot"})
slotval = slotelem.find("div", {"class": "pi-data-value pi-font"}).decode_contents()

typeelem = soup.find("div", {"data-source": "type"})
typevalmid = typeelem.find("div", {"class": "pi-data-value pi-font"})
typeval = typevalmid.find("a").get_text()

print("Name", nameval)
print("Mastery Rank Required:", masteryval)
print("Slot:", slotval)
print("Type:", typeval)

firerateelem = soup.find("div", {"data-source": "fire rate"})
firerateval = firerateelem.find(
    "div", {"class": "pi-data-value pi-font"}
).decode_contents()

findmgelem = soup.find("div", {"data-source": "finisher damage"})
findmgval = findmgelem.find("div", {"class": "pi-data-value pi-font"}).decode_contents()

chandmgelem = soup.find("div", {"data-source": "channel damage"})
chandmgval = chandmgelem.find(
    "div", {"class": "pi-data-value pi-font"}
).decode_contents()

blokreselem = soup.find("div", {"data-source": "block resist"})
blokresval = blokreselem.find(
    "div", {"class": "pi-data-value pi-font"}
).decode_contents()

dispoelem = soup.find("div", {"data-source": "disposition"})
dispomid = dispoelem.find("div", {"class": "pi-data-value pi-font"})
dispoval = dispomid.find("div").decode_contents()

impactelem = soup.find("td", {"data-source": "normal impact"}).get_text()
impactval = impactelem.strip()

punctelem = soup.find("td", {"data-source": "normal puncture"}).get_text()
punctval = punctelem.strip()

slashelem = soup.find("td", {"data-source": "normal slash"}).get_text()
slashval = slashelem.strip()

totaldmgelem = soup.find("div", {"data-source": "normal damage"})
totaldmgmid = totaldmgelem.find("div", {"class": "pi-data-value pi-font"}).get_text()
totaldmgval = totaldmgmid[0:4].strip()  # needs fix so no parentheses appears

print("Fire Rate:", firerateval)
print("Finisher Damage:", findmgval)
print("Channeling Damage:", chandmgval)
print("Block Resist:", blokresval)
print("Riven Despacito:", dispoval)
print("Impact:", impactval)
print("Puncture:", punctval)
print("Slash:", slashval)
print("Total Damage:", totaldmgval)

critchelem = soup.find("div", {"data-source": "normal critical chance"})
critchval = (
    critchelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
)

critdmgelem = soup.find("div", {"data-source": "normal critical damage"})
critdmgval = (
    critdmgelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
)

statuschelem = soup.find("div", {"data-source": "normal status chance"})
statuschval = (
    statuschelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
)

slamelem = soup.find("div", {"data-source": "slam attack"})
slamval = slamelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
slideelem = soup.find("div", {"data-source": "slide attack"})
slideval = slideelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()
wallelem = soup.find("div", {"data-source": "wall attack"})
wallval = wallelem.find("div", {"class": "pi-data-value pi-font"}).get_text().strip()

print("Critical Chance:", critchval)
print("Critical Damage:", critdmgval)
print("Status Chance:", statuschval)
print("Slam Attack:", slamval)
print("Slide Attack:", slideval)
print("Wall Attack:", wallval)
