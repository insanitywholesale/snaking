#!/usr/bin/env python3
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys
import requests


class MyW(QMainWindow):
    def __init__(self):
        super(MyW, self).__init__()
        self.setGeometry(500, 400, 300, 300)
        self.setWindowTitle("lister client")
        self.initUI()

    def initUI(self):
        # label
        self.l = QtWidgets.QLabel(self)  # self is QMainWindow
        self.l.setText("lists")
        self.l.move(50, 50)

        # button
        self.b = QtWidgets.QPushButton(self)  # self arg is QMainWindow
        self.b.setText("get lists")
        self.b.clicked.connect(self.clickyboi)

    def clickyboi(self):
        self.l.setText("da button got da press")
        self.update()
        # r = requests.get('http://localhost:9392/api/v1/lists')
        # print(r.content)#returns correct stuff and start with some b thing
        # print(r.json())#returns json object

    def update(self):
        self.l.adjustSize()  # without this the label can't accept the wider text after the change


def winder():
    # setup stuff
    a = QApplication(sys.argv)
    w = MyW()

    # show the window
    w.show()
    # graceful exit
    sys.exit(a.exec_())


winder()
